# Soal-Shift-Sisop-Modul-1-A13-2022

## daftar isi ##
- [Daftar Anggota Kelompok](#daftar-anggota-kelompok)
- [Nomer 1](#nomer-1)
    - [Soal  1.A](#soal-1a)
    - [Soal  1.B](#soal-1b)
    - [Soal  1.C](#soal-1c)
    - [Soal  1.D](#soal-1d)
- [Nomer 2](#nomer-2)
    - [Soal  2.A](#soal-2a)
    - [Soal  2.B](#soal-2b)
    - [Soal  2.C](#soal-2c)
    - [Soal  2.D](#soal-2d)
    - [Soal  2.E](#soal-2e)
- [Nomer 2](#nomer-3)
    - [Soal  3.A](#soal-3a)
    - [Soal  3.B](#soal-3b)
    - [Soal  3.C](#soal-3c)
    - [Soal  3.D](#soal-3d)
## Daftar Anggota Kelompok ##

NRP | NAMA | KELAS
--- | --- | ---
5025201004  | Danial Farros Maulana | SISOP C
5025201038    | Cholid Junoto | SISOP A
5025201220    | Davian Benito | SISOP A

## Nomer 1 ##
Pada suatu hari, Han dan teman-temannya diberikan tugas untuk mencari foto. Namun, karena laptop teman-temannya rusak ternyata tidak bisa dipakai karena rusak, Han dengan senang hati memperbolehkan teman-temannya untuk meminjam laptopnya. Untuk mempermudah pekerjaan mereka, Han membuat sebuah program.

### Soal 1.a ###
Han membuat sistem register pada script register.sh dan setiap user yang berhasil didaftarkan disimpan di dalam file ./users/user.txt. Han juga membuat sistem login yang dibuat di script main.sh

Penyelesaian untuk penyimpanan data user yang berhasil didaftarkan pada script register.sh, kita dapat meminta input dari user untuk "username" dan "password", lalu menyimpan nilai tersebut ke dalam path file ./users/user.txt.

```bash
echo "$username : $password" >> ./users/user.txt
```

Kemudian untuk sistem login pada script main.sh, kita harus mengecek apakah input username dan password yang didapat dari user sesuai dengan record file yang ada di ./users/user.txt sebelumnya. Untuk permasalahan ini, proses penyelesaian dilakukan dengan beberapa tahapan sebagai berikut:

1. Memasukkan data file record yang sesuai dengan input username ke dalam sebuah variable
```bash
existUsername=$(grep -w "$username" ./users/user.txt)
```
2. Lalu kita mengecek apakah data file record terambil, apabila tidak terambil maka variable akan kosong dan kita dapat melakukan while loops meminta ulang input username yang pada user sampai variable tadi memiliki nilai, yang artinya nilai username yang diinput ada pada file record
```bash
while [ -z "$existUsername" ]
do
	echo "Username not identified, please try again"
	echo -n "Username: "
	read username
	existUsername=$(grep -w "$username" ./users/user.txt)
done
```
3. Nantinya data yang didapatkan sesuai dengan syntax masukan pada register.sh yaitu:
"UserName : Password"
Sehingga kita dapat mengambil nilai password yang sesuai dengan data tersebut dengan cara

```bash
existPassword=$(echo $existUsername | awk '{print $3}')
```
4. Selanjutnya kita menerima input password dari user dan mengecek apakah input tersebut sesuai dengan nilai password aslinya. Bila tidak, kita lakukan while loops lagi

```bash
until [ $password == $existPassword ]
do
	echo "Password incorrect, please try again"
	echo "$(date +"%D %T") LOGIN: ERROR Failed login attempt on user $username" >> log.txt
	echo -n "Password: "
	read -s password
done
```

### Soal 1.b ###
Demi menjaga keamanan, input password pada login dan register harus tertutup/hidden dan password yang didaftarkan memiliki kriteria sebagai berikut
i.	Minimal 8 karakter
ii.	Memiliki minimal 1 huruf kapital dan 1 huruf kecil
iii.	Alphanumeric 
iv. Tidak boleh sama dengan username


Untuk persyaratan password harus hidden, dapat kita lakukan dengan menggunakan syntax read -s yang berarti silent
```bash
read -s password
```

Kemudian untuk kriteria persyaratan password, dapat kita lakukan dengan until loops sampai seluruh kriteria terpenuhi dengan logic
```bash
until [[ $password =~ [A-Z] && $password =~ [a-z] && $password =~ [0-9] && $password != *[^[:alnum:]]* && "$lowerPassword" != *"$lowerUsername"* && ${#password} -ge 8 ]]
do
	echo "Password minim should have capital and lower case letter and number, also can't have special character and can't be the same with the username"
	echo -n "Password: "
	read -s password
	lowerPassword=$(echo $password | tr '[:upper:]' '[:lower:]')
done
```
Penjelasan :
1. $password =~ [A-Z] && $password =~ [a-z] && $password =~ [0-9] && $password != *[^[:alnum:]]*
-untuk memastikan password terdiri dari alphanumeric saja(huruf, angka, bukan special character) dan juga harus minimal ada huruf kecil, huruf besar dan angka
2. ${#password} -ge 8
-Memastikan jumlah karakter password lebih besar sama dengan 8
3. "$lowerPassword" != *"$lowerUsername"*
-Memastikan password tidak mengandung unsur dari username. Untuk logic ini juga dibantu dengan menlowerkan kedua nilai password dan username
```bash
lowerUsername=$(echo $username | tr '[:upper:]' '[:lower:]')
lowerPassword=$(echo $password | tr '[:upper:]' '[:lower:]')
```

### Soal 1.c ###
Setiap percobaan login dan register akan tercatat pada log.txt dengan format : MM/DD/YY hh:mm:ss MESSAGE. Message pada log akan berbeda tergantung aksi yang dilakukan user.

i.Ketika mencoba register dengan username yang sudah terdaftar, maka message pada log adalah REGISTER: ERROR User already exists
ii.Ketika percobaan register berhasil, maka message pada log adalah REGISTER:
INFO User USERNAME registered successfull 
iii. Ketika user mencoba login namun passwordnya salah, maka message pada log adalah LOGIN: ERROR Failed login attempt on user USERNAME
iv.Ketika user berhasil login, maka message pada log adalah LOGIN: INFO User
USERNAME logged in


Untuk mendapatkan format MM/DD/YY hh:mm:ss MESSGAE dan memasukkannya ke file log.txt, dapat kita gunakan syntax shell:
```bash
echo "$(date +"%D %T") MESSAGE" >> log.txt
```

Lalu kita masukkan message yang sesuai untuk setiap aksi yang dilakukan user

### Soal 1.d ###
Setelah login, user dapat mengetikkan 2 command dengan dokumentasi sebagai berikut :

i.dl N ( N = Jumlah gambar yang akan didownload)
Untuk mendownload gambar dari https://loremflickr.com/320/240 dengan jumlah sesuai dengan yang diinputkan oleh user. Hasil download akan dimasukkan	ke	dalam	folder	dengan	format	nama
YYYY-MM-DD_USERNAME. Gambar-gambar yang didownload juga memiliki format nama PIC_XX, dengan nomor yang berurutan (contoh : PIC_01, PIC_02, dst. ). Setelah berhasil didownload semua, folder akan otomatis di zip dengan format nama yang sama dengan folder dan dipassword sesuai dengan password user tersebut. Apabila sudah terdapat file zip dengan nama yang sama, maka file zip yang sudah ada di unzip terlebih dahulu, barulah mulai ditambahkan gambar yang baru, kemudian folder di zip kembali dengan password sesuai dengan user. 
ii. att
Menghitung jumlah percobaan login baik yang berhasil maupun tidak dari user yang sedang login saat ini.


Untuk melihat command yang diberi user, dapat menggunakan while loops, kemudian menyimpan  nilai command yang user berikan ke dalam variable dan mengeceknya.
Untuk kasus commandnya:

-dl N

Dalam penyelesaian kasus ini, terdapat beberapa langkah yaitu:
1. Pertama kita tes dulu apakah user yang masuk telah memiliki folder zip syntax. Bila belum maka buat folder sesuai syntax user dan bila sudah maka unzip folder syntax user tersebut.
```bash
checkFolder=$(ls | grep -w "$(date +"%Y-%m-%d")_$username.zip")
        if [ -z "$checkFolder" ]
        then
            mkdir $(date +"%Y-%m-%d")_$username
        else
            unzip -P $password $(date +"%Y-%m-%d")_$username
```
2. Kemudian kita juga harus menghitung jumlah file gambar yang ada pada folder syntax username. Tujuan dilakukan ini, agar dapat memberikan penamaan yang benar pada file baru yang akan didownload. 
```bash
totalFile=$(ls ./$(date +"%Y-%m-%d")_$username | wc -l)
```
3. Lalu, lakukan download file sebanyak N dan diberi nama PIC_N lalu dimasukkan ke dalam folder syntax dan dizip dengan password.
```bash
for ((i=1+$totalFile; i<=$N+$totalFile; i=i+1))
		do
			curl -L https://loremflickr.com/320/240 -o PIC_$i
			mv PIC_$i $(date +"%Y-%m-%d")_$username
		done
		zip -r $(date +"%Y-%m-%d")_$username $(date +"%Y-%m-%d")_$username
```
-att

Dalam penyelesaian kasus ini, kita dapat mencari jumlah login username dari jumlah output baris pada log.txt dengan nama $username. 
```bash
totalLogins=$(grep -w "$username" log.txt | wc -l)
let $totalLogins-1
echo "Logins Attempted: $totalLogins"
```
totalLogin dikurangi 1 karena pada log.txt juga terdapat catatan nama $username untuk peristiwa registered pertama kali. Sehingga kita kurangi 1 dari jumlah total yang didapat.


## Nomer 2 ##
Pada tanggal 22 Januari 2022, website https://daffa.info di hack oleh seseorang yang tidak bertanggung jawab. Sehingga hari sabtu yang seharusnya hari libur menjadi berantakan. Dapos langsung membuka log website dan menemukan banyak request yang berbahaya. Bantulah Dapos untuk membaca log website https://daffa.info Buatlah sebuah script awk bernama "soal2_forensic_dapos.sh" untuk melaksanakan tugas-tugas berikut:

### Soal 2.a ###
Buat folder terlebih dahulu bernama forensic_log_website_daffainfo_log.
penyelesaian
```bash
mkdir forensic_log_website_daffainfo_log  
```
mkdir merupakan syntax dari bash yang digunakan untuk membuat folder
### Soal 2.b ### 
Dikarenakan serangan yang diluncurkan ke website https://daffa.info sangat banyak, Dapos ingin tahu berapa rata-rata request per jam yang dikirimkan penyerang ke website. Kemudian masukkan jumlah rata-ratanya ke dalam sebuah file bernama ratarata.txt ke dalam folder yang sudah dibuat sebelumnya.
```bash awk
cd forensic_log_website_daffainfo_log
cat log_website_daffainfo.log | awk '{split($0,a,":"); 
	b[a[2]":"a[3]"\""]++;}END{
		for(i in b) { 
		    count++; 
		    summ+=b[i];}
		    summ = summ/count;
		printf "rata rata serangan perjam adalah sebanyak %.3f request per jam", summ;
		}' > ratarata.txt  
```
pada permasalahan ini proses penyelesaian dilakukan dengan beberapa tahapan
1. memasuki folder yang digunakan dengan menggunakan perintah `cd` 
2. berikut adalah sampel data yang akan diolah `"167.248.133.59":"22/Jan/2022:00:46:58":"GET / HTTP/1.1":200:963:"-"` pada data ini terdapat keunikan dimana setiap bagian dipisahkan dengan tanda titik dua `:`, sehingga dapat digunakan untuk mempermudah `awk` dalam proses spliting.
3. berdasar hasil spliting didapatkan bahwa :
    - nilai a[1] merupakan data IP
    - nilai a[2] merupakan data tanggal (dd/mmm/yyyy)
    - nilai a[3] merupakan data jam (hh)
    - nilai a[4] merupakan data menit (mm)
    - nilai a[5] merupakan data detik (ss)
    - nilai a[6] merupakan data request
    - nilai a[7] merupakan data status code
    - nilai a[8] merupakan data content length
    - nilai a[9] merupakan datauser agent
sehingga gabungan untuk nilai `a[2] + a[3]` dapat digunakan untuk membuat indexing jam
4. proses perhitungan banyaknya request setiap jam dilakukan dengan membuat aray b dengan index [a[2]":"a[3] dengan proses perhitungan jumlah jam akan dilakukan penambahan dengan syntax `++` jika ditemukan nilai index array yang sama
5. inti dari soal ini adalah melakukan perhitungan rata rata reqest yang ada pada setiap jam sehingga di akhir awk (end) dilakukan looping sebanyak array b untuk menghitung jumlah array b dan jumlah total dari setiap nilai dari array b
6. proses penyimpanan output pada text dilakukan denga sytax `> ratarata.txt` sesuai dengan output yang di minta
### Soal 2.c ###
Sepertinya penyerang ini menggunakan banyak IP saat melakukan serangan ke website https://daffa.info, Dapos ingin menampilkan IP yang paling banyak melakukan request ke server dan tampilkan berapa banyak request yang dikirimkan dengan IP tersebut. Masukkan outputnya kedalam file baru bernama result.txt kedalam folder yang sudah dibuat sebelumnya.
```bash 
cat log_website_daffainfo.log | awk '{split($0,a,":"); 
	b[a[1]]++;}END{
		max = 0;
		target;
		for(i in b) {
			if (max < b[i]){
				target = i;
				max = b[target]
			}
		}
		print "yang paling banyak menagkses server adalah : " target " sebanyak " max " request\n"
		}' > result.txt
```
berdasar dari penjelasan soal nomer 2.b nilai array a[1] akan memberikan data IP sehingga untuk menghitung Request IP dengan terbanyak dapat dilakukan dengan :
1. membuat array b dengan index a[1]
2. setiap iterasi dilakukan penambahan jumlah nilai pada array b[x] Jika ditemukan index IP yang sama
3. inti dari soal ini adalah mencari index IP dengan jumlah request terbanyak, sehingga di akhir awk (end) akan diakukan looping untuk mencari nilai array terbesar
4. proses penyimpanan output pada text dilakukan dengan sytax `>> result.txt` sesuai dengan output yang diminta
### Soal 2.d ###
Beberapa request ada yang menggunakan user-agent ada yang tidak. Dari banyaknya request, berapa banyak requests yang menggunakan user-agent curl?
Kemudian masukkan berapa banyak requestnya kedalam file bernama result.txt yang telah dibuat sebelumnya.
```bash
cat log_website_daffainfo.log  | awk '/curl/  { ++n }
END   { print "ada", n, "request yang menggunakan curl sebagai user-agent\n" }' >> result.txt
```
untuk mencari requests yang menggunakan user-agent curl digunakan syntax awk dengan streategi sebagi berikut:
1. jika ditemukan string dengan nilai `/curl/` akan dilakukan penambahan nilai pada index `n`
2. inyi dari soal ini adalah melakukan perhitungan banyaknya `curl` yang muncul pada log website sehingga outputan akan disimpan pada file dengan syntax `> result.txt`, pada soal ini digunakan `>>` karena digunakan untuk menambahkan outputan pada baris selanjutnya
### Soal 2.e ###
Pada jam 2 pagi pada tanggal 22 terdapat serangan pada website, Dapos ingin mencari tahu daftar IP yang mengakses website pada jam tersebut. Kemudian masukkan daftar IP tersebut kedalam file bernama result.txt yang telah dibuat sebelumnya.
```bash
cat log_website_daffainfo.log | awk '/22\/Jan\/2022:02/{
	split($0,a,":")
	b[a[1]]++;
	count++}END{
		for (i in b){
			print i " IP Address Jam 2 Pagi"
		}
	}' >> result.txt
```
berdasar penjelasan soal nomer 2.e soal ini bertujuan untuk mencari ip mana saja yang melakukan request pada jam 02 tanggal 22 Januari 2022 sehingga proses ini dapat dilakukan dengan :
1. mencari string dengan nilai /22\/Jan\/2022:02/ jika ditemukan akan melakukan spliting dengan dengan membagi setiap char ':'
2. untuk menampilkan ip tanpa pengulangan dibuat array b dengan index a[1]
3. inti dari soal ini adalah menampilkan IP mana saja yang melakukan request pada jam 2 pagi.
4. proses penyimpanan outputan akan disimpan pada file dengan syntax `>> result.txt`, pada soal ini digunakan `>>` karena digunakan untuk menambahkan outputan pada baris selanjutnya

## Nomer 3 ##
Ubay sangat suka dengan komputernya. Suatu saat komputernya crash secara tiba-tiba :(. Tentu saja Ubay menggunakan linux. Akhirnya Ubay pergi ke tukang servis untuk memperbaiki laptopnya. Setelah selesai servis, ternyata biaya servis sangatlah mahal sehingga ia harus menggunakan dana kenakalannya untuk membayar biaya servis tersebut. Menurut Mas Tukang Servis, laptop Ubay overload sehingga mengakibatkan crash pada laptopnya. Karena tidak ingin hal serupa terulang, Ubay meminta kalian untuk membuat suatu program monitoring resource yang tersedia pada komputer.

Buatlah program monitoring resource pada komputer kalian. Cukup monitoring ram dan monitoring size suatu directory. Untuk ram gunakan command `free -m`. Untuk disk gunakan command `du -sh <target_path>`. Catat semua metrics yang didapatkan dari hasil `free -m`. Untuk hasil `du -sh <target_path>` catat size dari path directory tersebut. Untuk target_path yang akan dimonitor adalah /home/{user}/.

### Soal 3.a ###
Masukkan semua metrics ke dalam suatu file log bernama metrics_{YmdHms}.log. {YmdHms} adalah waktu disaat file script bash kalian dijalankan. Misal dijalankan pada 2022-01-31 15:00:00, maka file log yang akan tergenerate adalah metrics_20220131150000.log.
```dash
now=$(date +"%Y%m%d%H%M%S")
output=`free -m | awk '/Mem:/ {print $2","$3","$4","$5","$6","$7}'`
output1=`free -m |awk '/:/{split($0,a," ");}END{print a[2]","a[3]","a[4]}'`
var2=`du -h`
var2=`echo "$var2"|awk '{split($0,a," ");}END{print a[1]}'`
echo $output","$output1","`pwd readlink -f test.sh`","$var2"," >> ~/log/metrics_$now.log

```
pada soal nomer 3.a dilakukan untuk mencatat beberap data yang sudah disebutkan dan disimpannya kedalam file, adapun langkah yang dapat dilakukan antara lain:
1. pencatatan waktu dilakukan dengan menggunakan fungsi date yang kemudian akan disimpan kedalam variabel now, adapun hasil yang didapat akan berupa `YYYMMDDHHMMSS`.
2. proses spliting data dari `free -m` dilakukan dengan fungsi awk yang selanjutnya akan di simpan kedalam variabel output dan output1.
3. proses pengambilan nilai path size digunakan comand `du -h` dan selanjutnya akan dilakukan spliting agar hasil yang didapatkan lebih rapi.
4. proses penggabungan output dilakukan dengan menggabungkan seluruh variabel yang ada dan disimpan pada folder yang sudah ditentukan.
### Soal 3.b ###
Script untuk mencatat metrics diatas diharapkan dapat berjalan otomatis pada setiap menit.
```bash
crontab -e
``` 
syntax diatas digunakan untuk membuka crontab. untuk dapat melakukan kerja otomatis pada file minute_log.sh digunakan syntax pada crontab sebagai berikut:
``` bash
* * * * * bash minute_log.sh
```
### Soal 3.c ###
Kemudian, buat satu script untuk membuat agregasi file log ke satuan jam. Script agregasi akan memiliki info dari file-file yang tergenerate tiap menit. Dalam hasil file agregasi tersebut, terdapat nilai minimum, maximum, dan rata-rata dari tiap-tiap metrics. File agregasi akan ditrigger untuk dijalankan setiap jam secara otomatis. Berikut contoh nama file hasil agregasi metrics_agg_2022013115.log dengan format metrics_agg_{YmdH}.log
```bash
now=$(date +"%Y%m%d")
now1=$(date +"%Y%m%d%H")

		echo `for ((num=1; num<10; num++))
			do
			cat ~/log/metrics_$now1$num"01".log 
			done
			for ((num=10; num<60; num++))
			do
			cat ~/log/metrics_$now1$num"01".log 
			done`| awk '{split($0,a,",");
		}{	
			max1=0;max2=0;max3=0;max4=0;max5=0;max6=0;max7=0;max8=0;max9=0;max10=0
			min1=99999;min2=99999;min3=99999;min4=99999;min5=99999;min6=99999;min7=99999;min8=99999;min9=99999;min10=99999
			for (i=1;i<7;i++){

			#sum handling
			ptsize=int(substr(a[(i-1)*11+11], 1, length(a[(i-1)*11+11])-1))
			sum1+=a[(i-1)*11+1];sum2+=a[(i-1)*11+2];sum3+=a[(i-1)*11+3];
			sum4+=a[(i-1)*11+4];sum5+=a[(i-1)*11+5];sum6+=a[(i-1)*11+6];
			sum7+=a[(i-1)*11+7];sum8+=a[(i-1)*11+8];sum9+=a[(i-1)*11+9];
			sum10+= ptsize;
			
			#find max handling
			if (max1<a[(i-1)*11+1]) max1 =a[(i-1)*11+1];
			if (max2<a[(i-1)*11+2]) max2 =a[(i-1)*11+2];
			if (max3<a[(i-1)*11+3]) max3 =a[(i-1)*11+3];
			if (max4<a[(i-1)*11+4]) max4 =a[(i-1)*11+4];
			if (max5<a[(i-1)*11+5]) max5 =a[(i-1)*11+5];
			if (max6<a[(i-1)*11+6]) max6 =a[(i-1)*11+6];
			if (max7<a[(i-1)*11+7]) max7 =a[(i-1)*11+7];
			if (max8<a[(i-1)*11+8]) max8 =a[(i-1)*11+8];
			if (max9<a[(i-1)*11+9]) max9 =a[(i-1)*11+9];
			if (max10<ptsize) max10 =ptsize;
			
			#find min handling
			if (min1>a[(i-1)*11+1]) min1 =a[(i-1)*11+1];
			if (min2>a[(i-1)*11+2]) min2 =a[(i-1)*11+2];
			if (min3>a[(i-1)*11+3]) min3 =a[(i-1)*11+3];
			if (min4>a[(i-1)*11+4]) min4 =a[(i-1)*11+4];
			if (min5>a[(i-1)*11+5]) min5 =a[(i-1)*11+5];
			if (min6>a[(i-1)*11+6]) min6 =a[(i-1)*11+6];
			if (min7>a[(i-1)*11+7]) min7 =a[(i-1)*11+7];
			if (min8>a[(i-1)*11+8]) min8 =a[(i-1)*11+8];
			if (min9>a[(i-1)*11+9]) min9 =a[(i-1)*11+9];
			if (min10>ptsize) min10 =ptsize;
			
			
			}
			print "minimum,"min1/1","min2","min3","min4","min5","min6","min7","min8","min9","a[10]","min10"K"
			print "maximum,"max1/1","max2","max3","max4","max5","max6","max7","max8","max9","a[10]","max10"K"
			print "Average,"sum1/60","sum2/60","sum3/60","sum4/60","sum5/60","sum6/60","sum7/60","sum8/60","sum9/60","a[10]","sum10"K"
		}' > ~/log/metrics_agg_$now1.log
	
for ((num=1301; num<1301; num++))
	do
		
		cat metrics_$now$num"01".txt | awk '{split($0,a,","); 
		}END{
			#print a[1], a[2];
			sum1+=a[1];sum2+=a[2];
			print sum1, sum2;
		}' 
		#> ratarata.txt
	done
```
pada soal nomer 3.c digunakan untuk mencari nilai minimum maximum dan rata rata dari file yang tergenerate setiap menitnya, adapun langkah langkahnya dapat dituliskan sebagai berikut:
1. proses berjalan soal ini menggunakan awk, adapun proses input dilakukan dengan syntax echo dan proses looping, mengingat semua file akan tergenerate pada detik `01` maka proses pemanggilan file cukup menggunakan nama file berupa `waktu(metrics_YYYYMMDDHH+'mm''01'.log) dimana nilai 'mm' akan dilakukan iterasi dari 1 ke 60
2. selanjutnya dari inputan yang ada akan dilakukan spliting jika ditemukan tanda komma `,`
3. pada setiap iterasi awk akan dilakukan pencarian nilai minimum, maximum dan menjumlahkan semua data yang ada.
4. pada akhir awk akan dilakukan proses penampilan output berupa nilai maximum minimum dan rata rata yang beruba hasil bagi total dengan 60
5. output yang ada kemudian akan disimpan pada file dan folder yang sudah ditentukan
6. untuk memastikan file ini dapat berjalan setiap jamnya, perlu di tambahkan penjadwaalan cron dengan sintax sebagai berikut
```bash
1 * * * * bash aggregate_minute_to_hourly_log.sh
```
### Soal 3.d ###
Karena file log bersifat sensitif pastikan semua file log hanya dapat dibaca oleh user pemilik file.
```bash 
#minute_log.sh
> ~/log/metrics_$now.log
#aggregate_minute_to_hourly_log.sh
> ~/log/metrics_agg_$now1.log
```
syntax diata peru ditambahkan di masing masing part setelah awk untuk memastikan file yang digunakan akan tersimpan pada folde `{user/log}`

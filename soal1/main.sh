#!/bin/bash
echo "---LOGIN PAGE---"

echo -n "Username: "
read username

existUsername=$(grep -w "$username" ./users/user.txt)

while [ -z "$existUsername" ]
do
	echo "Username not identified, please try again"
	echo -n "Username: "
	read username
	existUsername=$(grep -w "$username" ./users/user.txt)
done

existPassword=$(echo $existUsername | awk '{print $3}')

echo -n "Password: "
read -s password

until [ $password == $existPassword ]
do
	echo "Password incorrect, please try again"
	echo "$(date +"%D %T") LOGIN: ERROR Failed login attempt on user $username" >> log.txt
	echo -n "Password: "
	read -s password
done

for ((num=1; num<=${#password}; num=num+1))
do
	echo -n "*"
done

echo "$(date +"%D %T") LOGIN: INFO User $username logged in" >> log.txt

echo -e "\n"
echo "---login success---"



read commandss
command=$(echo $commandss | awk '{print $1}')

while [ $command != "exit" ]
do
	if [ $command == "dl" ]
	then
		totalFile=0
		checkFolder=$(ls | grep -w "$(date +"%Y-%m-%d")_$username.zip")
		if [ -z "$checkFolder" ]
		then
			mkdir $(date +"%Y-%m-%d")_$username
		else
			unzip -P $password $(date +"%Y-%m-%d")_$username
			totalFile=$(ls ./$(date +"%Y-%m-%d")_$username | wc -l)
		fi

		N=$(echo $commandss | awk '{print $2}')
		for ((i=1+$totalFile; i<=$N+$totalFile; i=i+1))
		do
			curl -L https://loremflickr.com/320/240 -o PIC_$i
			mv PIC_$i $(date +"%Y-%m-%d")_$username
		done
		zip -P $password -r $(date +"%Y-%m-%d")_$username $(date +"%Y-%m-%d")_$username
		rm -r $(date +"%Y-%m-%d")_$username
		
	elif [ $command == "att" ]
	then
		totalLogins=$(grep -w "$username" log.txt | wc -l)
		let $totalLogins-1
		echo "Logins Attempted: $totalLogins"
	else
		echo "command : dl N > download N pictures   |   att > login attempts   |   exit > quit program"
	fi
	
	read commandss
	command=$(echo $commandss | awk '{print $1}')
done

#!/bin/bash
echo "---REGISTER PAGE---"

echo -n "Username: "
read username

existFolder=$(ls | grep -w "users")
if [ -z "$existFolder" ]
then
	mkdir users
	> user.txt
	mv user.txt ./users
fi

existUsername=$(grep -w "$username" ./users/user.txt)

until [ -z "$existUsername" ]
do
	echo "Username already taken, please try again"
	echo "$(date +"%D %T") REGISTER: ERROR User already exists" >> log.txt
	echo -n "Username: "
	read username
	existUsername=$(grep -w "$username" ./users/user.txt)
done


echo -n "Password: "
read -s password

lowerUsername=$(echo $username | tr '[:upper:]' '[:lower:]')
lowerPassword=$(echo $password | tr '[:upper:]' '[:lower:]')

until [[ $password =~ [A-Z] && $password =~ [a-z] && $password =~ [0-9] && $password != *[^[:alnum:]]* && "$lowerPassword" != *"$lowerUsername"* && ${#password} -ge 8 ]]
do
	echo "Password minim should have capital and lower case letter and number, also can't have special character and can't be the same with the username"
	echo -n "Password: "
	read -s password
	lowerPassword=$(echo $password | tr '[:upper:]' '[:lower:]')
done

for ((num=1; num<=${#password}; num=num+1))
do
	echo -n "*"
done

echo "$(date +"%D %T") REGISTER: INFO User $username registered successfully" >> log.txt

echo -e "\n"
echo "---REGISTER SUCCESSFUL---"
echo "$username : $password" >> ./users/user.txt

